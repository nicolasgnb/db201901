import psycopg2
import requests
import urllib.request
import time
import re
import datetime
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup

QUERY_FILM = """SELECT movieuri FROM FILME"""
QUERY_BANDS = """SELECT banduri FROM ARTISTA_MUSICAL"""
UPDATE_FILM_QUERY = """UPDATE filme 
                                SET nome = '{0}', 
                                data_lancamento = '{1}', 
                                diretor = '{2}',
                                rating = '{3}',
                                awards = '{4}'
                            WHERE movieuri = '{5}';"""

INSERT_CATEGORIA_QUERY = "INSERT INTO categoria (nome) VALUES('{}');"
INSERT_DIRECTOR_QUERY = "INSERT INTO diretor VALUES ('{}');"
INSERT_ARTISTA_FILME_QUERY = "INSERT INTO artista_filme VALUES ('{}', 0, 'street');"
INSERT_PERCENTE_QUERY = "INSERT INTO pertence VALUES ('{0}', '{1}');"
INSERT_GENERO_MUSICAL_QUERY = "INSERT INTO genero_musical (nome) VALUES('{}');"
INSERT_PERTENCE_MUSICAL_QUERY = "INSERT INTO pertence_musical (artista, genero) VALUES('{0}','{1}');"

UPDATE_ARTIST_QUERY = """ UPDATE artista_musical
                            SET pais = '{0}',
                            nome_artistico = '{1}'
                            WHERE banduri = '{2}';"""

BROKEN = ['aaaaaaa',
          'https://en.wikipedia.org/wiki/Pyotr_Ilyich_Tchaikovsky',
          'https://en.wikipedia.org/wiki/Nirvana',
          'https://en.wikipedia.org/wiki/Khalid',
          'https://en.wikipedia.org/wiki/Dragon_Force',
          'https://en.wikipedia.org/wiki/J._Cole',
          'https://en.wikipedia.org/wiki/Madonna',
          'https://en.wikipedia.org/wiki/Two_Steps_from_Hell',
          'https://en.wikipedia.org/wiki/Fiddler%27s_Green',
          'https://en.wikipedia.org/wiki/Sticky_Fingers',
          'https://en.wikipedia.org/wiki/Yanni',
          'https://en.wikipedia.org/wiki/Post_Malone'
          'https://en.wikipedia.org/wiki/Marshmello',
          'https://en.wikipedia.org/wiki/Adele',
          'https://en.wikipedia.org/wiki/Lil_Pump']


def execute_query(conn, query):
    try:
        conn.cursor().execute(query)
        conn.commit()
    except psycopg2.Error as e:
        print(e)


def set_movie(conn, nome, data, diretor, uri, categorias, rating, awards):
    execute_query(conn, INSERT_ARTISTA_FILME_QUERY.format(diretor))
    execute_query(conn, INSERT_DIRECTOR_QUERY.format(diretor))
    for categoria in categorias:
        execute_query(conn, INSERT_CATEGORIA_QUERY.format(categoria))
        execute_query(conn, INSERT_PERCENTE_QUERY.format(uri, categoria))
    execute_query(conn, UPDATE_FILM_QUERY.format(
        nome, data, diretor, rating, awards, uri))


def get_movies(cursor):
    cursor.execute(QUERY_FILM)
    return cursor.fetchall()


def get_bands(cursor):
    cursor.execute(QUERY_BANDS)
    return cursor.fetchall()


def get_movieinfo(movie):
    id = movie.split('title/')[1].replace("/", "")
    info = requests.get(
        "http://www.omdbapi.com/?apikey=8d4194f4&i={}".format(id))
    return info.json()


def get_genre(soup):
    genres = set()
    for a in soup.find("th", text=re.compile(r'Genres')).nextSibling.findChildren("a"):
        genres.add(a.text.strip())
    return genres


def get_bandinfo(band):
    try:
        response = requests.get(band)
        soup = BeautifulSoup(response.text, 'html.parser')
        name = soup.find("div", {"class": "fn"}).text
        try:
            origin = soup.find("th", text=re.compile(
                r'Origin')).nextSibling.text.split("[")[0]
        except AttributeError as error:
            try:
                origin = soup.find("th", text=re.compile(
                    r'Born')).nextSibling.a.text.split("[")[0]
            except AttributeError as error:
                origin = None

        genres = set()
        try:
            for a in soup.find("th", text=re.compile(r'Genres')).nextSibling.findChildren("a"):
                if '[' not in a.text:
                    genres.add(a.text.strip())
        except AttributeError as error:
            pass

        return (name, origin, genres)
    except requests.exceptions.MissingSchema as error:
        print(error)


def update_filmes(conn, info, uri):
    title = re.escape(info['Title'])
    date = datetime.datetime.strptime(
        info['Released'], '%d %b %Y').strftime('%Y-%m-%d')
    director = info['Director'].split(",")[0]
    genre = info['Genre'].split(", ")
    rated = info['Rated']
    awards = info['Awards']
    set_movie(conn, title, date, director, uri, genre, rated, awards)


def set_artist_genre(conn, genres, artist):
    for genre in genres:
        execute_query(conn, INSERT_GENERO_MUSICAL_QUERY.format(genre))
        execute_query(
            conn, INSERT_PERTENCE_MUSICAL_QUERY.format(artist, genre))


def update_artistas(conn, info, uri):
    set_artist_genre(conn, info[2], uri)
    name = re.escape(info[0])
    origin = info[1]
    execute_query(conn, UPDATE_ARTIST_QUERY.format(origin, name, uri))


def add_movie_xml(movie, uri, xml):
    title = re.escape(movie['Title']).replace('\\', '')
    date = datetime.datetime.strptime(
        movie['Released'], '%d %b %Y').strftime('%Y-%m-%d')
    director = movie['Director'].split(",")[0]
    genre = movie['Genre']
    rated = movie['Rated']
    awards = movie['Awards']
    ET.SubElement(xml, "movie", uri=uri, nome=title,
                  data_lancamento=date, diretor=director, genero=genre, classificacao=rated, premios=awards)


def add_band_xml(band, uri, xml):
    nome = band[0]
    origin = band[1] if band[1] != None else ''
    generos = ', '.join(band[2]) if len(band[2]) > 0 else ''
    ET.SubElement(xml, "band", uri=uri, nome=nome, origem=origin, generos=generos)


# select * from filme;
if __name__ == "__main__":
    try:
        connection = psycopg2.connect(
            dbname="1901Team_One",
            host="200.134.10.32",
            user="1901Team_One",
            password="471737",
        )
        connection.autocommit = True
        cursor = connection.cursor()

        movies = get_movies(cursor)
        moviesXML = ET.Element("movies")
        for movie in movies:
            info = get_movieinfo(movie[0])
            update_filmes(connection, info, movie[0])
            add_movie_xml(info, movie[0], moviesXML)
        bands = get_bands(cursor)
        bandsXML = ET.Element("bands")
        for band in bands:
            if band[0] not in BROKEN:
                info = get_bandinfo(band[0])
                update_artistas(connection, info, band[0])
                add_band_xml(info, band[0], bandsXML)

        tree = ET.ElementTree(moviesXML)
        tree.write("movie.xml")

        tree = ET.ElementTree(bandsXML)
        tree.write("music.xml")

        connection.commit()
    except psycopg2.Error as error:
        print(error.pgerror)
    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("Conexão fechada.")
