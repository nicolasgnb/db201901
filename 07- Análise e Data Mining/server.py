from flask import Flask, Response
from flask_cors import CORS, cross_origin
from flask_restful import Api, Resource, reqparse
import json
import requests
from movie import MatrixFactorizationRec
from music import MusicRecommender
from scraping import searchArtist

app = Flask(__name__)
cors = CORS(app)
api = Api(app)

movieRecommender = MatrixFactorizationRec('latin-1', 'ml-latest-small/')
musicRecommender = MusicRecommender('lastFM-data/')

def get_movieinfo(imdbId):
    if(len(imdbId) == 6):
        imdbId = "0" + imdbId
    info = requests.get(
        "http://www.omdbapi.com/?apikey=8d4194f4&i=tt{}".format(imdbId))
    return info.json()

class GenreRecommendation(Resource):
    def get(self, title):
        recs = movieRecommender.genre_recommendations(title)
        movies = []
        for imdbId in recs:
            movies.append(get_movieinfo(imdbId))
        resp = Response(json.dumps(movies))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

class Recommendation(Resource):
    def get(self, userId):
        recs = movieRecommender.recommend_movies(userId)
        movies = []
        for imdbId in recs['imdbId']:
            movies.append(get_movieinfo(imdbId))
        resp = Response(json.dumps(movies))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

class UserMusicRecommendation(Resource):
    def get(self, userId):
        recommendations = musicRecommender.userRec(userId)
        recs = []
        for rec in recommendations:
            try:
                recs.append(searchArtist(rec))
            except:
                pass
        resp = Response(json.dumps(recs))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

class MusicRecommendation(Resource):
    def get(self, movieTitle):
        recommendations = musicRecommender.itemRec(movieTitle, k=10)
        recs = []
        broken = 0
        for rec in recommendations:
            try:
                if(len(recs) < 5):
                    recs.append(searchArtist(rec))
                else:
                    break
            except:
                broken += 1
                if broken >= 5:
                    break
                pass
        resp = Response(json.dumps(recs))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp

api.add_resource(Recommendation, "/movie/<userId>")
api.add_resource(GenreRecommendation, "/movie/genre/<title>")
api.add_resource(MusicRecommendation, "/music/title/<movieTitle>")
api.add_resource(UserMusicRecommendation, "/music/<userId>")

app.run(debug=True)