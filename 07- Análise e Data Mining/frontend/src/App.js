import React from "react";
import ItemContainer from "./ItemContainer";
import "./App.css";

function App() {
  const pStyle = {
    fontSize: "20px",
    color: "white",
    fontWeight: 600,
    marginTop: '2em',
  };
  return (
      <div className="container">
        <p style={pStyle}>Filmes</p>
        <ItemContainer
          type="movie"
          placeholder="Usuário"
          url="http://localhost:5000/movie"
        />
        <ItemContainer
          placeholder="Filme"
          type="movie"
          url="http://localhost:5000/movie/genre"
        />
        <p style={pStyle}>Artistas músicais e bandas</p>
        <ItemContainer
          type="music"
          placeholder="Usuario"
          url="http://localhost:5000/music"
        />
        <ItemContainer
          type="music"
          placeholder="Artista musical"
          url="http://localhost:5000/music/title"
        />
      </div>
  );
}

export default App;
