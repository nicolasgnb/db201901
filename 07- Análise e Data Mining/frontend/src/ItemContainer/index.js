import React, { useState } from "react";
import axios from "axios";
import { Form, Grid, Icon, Message } from "semantic-ui-react";

function MovieContainer(props) {
  const { url, placeholder, type } = props;
  const [itemSearch, setItemSearch] = useState("");
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [items, setItems] = useState({});
  const fetchItems = async () => {
    try {
      const response = await axios.get(`${url}/${itemSearch}`, {
        headers: { "Access-Control-Allow-Origin": "*" }
      });
      setItems(response.data);
      setLoading(false);
    } catch (error) {
      setError(true);
      setTimeout(() => setError(false), 3000);
      setLoading(false);
      console.log(error);
    }
  };

  const onSubmit = event => {
    event.preventDefault();
    setLoading(true);
    setError(false);
    fetchItems();
  };
  const inputClass = !!loading
    ? "ui loading action left icon input"
    : "ui action input";
  return (
    <>
      <Form onSubmit={onSubmit} style={{ marginTop: "1em" }}>
        <div className={inputClass}>
          <input
            type="text"
            placeholder={placeholder}
            value={itemSearch}
            onChange={fieldValue => setItemSearch(fieldValue.target.value)}
          />
          {!!loading && <i aria-hidden="true" className="user icon" />}
          <button type="submit" className="ui button">
            Search
          </button>
        </div>
      </Form>
      {!!error && (
        <Message negative>
          <Message.Header>Ocorreu um erro!</Message.Header>
        </Message>
      )}
      {!!Array.from(items).length && !loading && (
        <Grid
          columns={Array.from(items).length < 5 ? Array.from(items).length : 5 }
          doubling
          style={{ marginTop: "2em", color: "white", textAlign: "center" }}
        >
          {items.map(item => {
            const isMovie = type === "movie";
            const src = isMovie ? item.Poster : item.cover_art;
            const key = isMovie ? item.imdbID : item.id;
            const title = isMovie ? item.Title : item.name;
            return (
              <Grid.Column key={key}>
                <a
                  href={
                    isMovie
                      ? `https://www.imdb.com/title/${item.imdbID}/`
                      : null
                  }
                >
                  <img src={src} alt={title} width="200px" />
                </a>
                <p>
                  <b>{title}</b>
                </p>
                {isMovie && (
                  <p>
                    <Icon name="star" color="yellow" />
                    {item.imdbRating}
                  </p>
                )}
              </Grid.Column>
            );
          })}
        </Grid>
      )}
    </>
  );
}

export default MovieContainer;
