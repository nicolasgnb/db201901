arquivos fontes: Todos na pasta raiz
    server.py - python que configura o server para o web service
    frontend/ - pasta com o projeto react do frontend para o webservice
    lastFM/ - pasta com dados obtidos do dataset lastFM para recomendação de artistas
    ml-latest-small/ - pasta com dados obtidos do dataset MovieLens para recomendação de filmes
    apresentacao/ - pasta com arquivo .tex e correlatos para criar os slides
    db_insert.py - faz a inserção de dados a partir do XML(com scraping) no banco de dados
    scraping.py - obtém informações relevantes para complementar o banco
    dataset_parser - extrai informações relevantes do dataset lastFM (usado apenas uma vez para gerar os arquivos em lastFM/)
    mining.py - faz a mineração de dados (primeira parte do trabalho) 
    music.py - faz a recomendação de artista musical
    movie.py - faz recomendação de filmes

Os pdfs também se encontram na pasta raiz
