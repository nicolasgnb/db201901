import pandas as pd
import csv 
import scraping
from db_insert import *

ARTIST_PLAYS_UPDATE = """
UPDATE ARTISTAMUSICAL
SET quantidade_tocado = %s
WHERE mbid = %s
"""

FIND_ARTIST = """
SELECT mbid FROM ARTISTAMUSICAL
WHERE mbid = %s
"""

def insertDB(filename):
    """Insere os dados contindos no arquivo filename no banco de dados"""
    artist_data = pd.read_csv(filename)
    try:
        connection = psycopg2.connect(
            dbname="1901Team_One",
            host="200.134.10.32",
            user="1901Team_One",
            password="471737",
        )

        connection.autocommit = True
    except psycopg2.Error as error:
        print(error)
    cursor = connection.cursor()
    
    for index, id in enumerate(artist_data["musicbrainz-artist-id"]):
        cursor.execute(FIND_ARTIST, (id,) )
        if cursor.rowcount == 0:
            artist = scraping.getArtistById(id)
            info, members = scraping.getArtistInfo(artist)
            insertMusicArtist(connection, info, members)
        cursor.execute(ARTIST_PLAYS_UPDATE, (int(artist_data["total_artist_plays"][index]),id))

def parse(filename_plays, filename_profiles):
    pd.set_option('display.float_format',lambda x : '%.3f' % x )
    
    user_data = pd.read_csv(filename_plays,
                            sep='\t',
                            header=None,
                            nrows=10e5,
                            names= ["users", "musicbrainz-artist-id","artist-name","plays"],
                            usecols= ["users", "musicbrainz-artist-id","artist-name","plays"]
                            )

    user_profiles = pd.read_csv(filename_profiles,
                                sep='\t',
                                header = None,
                                names = ['users', 'gender', 'age', 'country', 'signup'],
                                usecols = ['users', 'country'])

    #drops broked data
    if user_data['artist-name'].isnull().sum() > 0:
        user_data = user_data.dropna(axis = 0, subset = ['artist-name'])
    if user_data['musicbrainz-artist-id'].isnull().sum() > 0:
        user_data = user_data.dropna(axis = 0, subset = ['musicbrainz-artist-id'])
    

    artist_plays = (user_data.
                    groupby(by = ['artist-name'])['plays'].sum().reset_index().
                    rename(columns = {'plays': 'total_artist_plays'})
                    [['artist-name', 'total_artist_plays']] )

    user_data_with_artist_plays = user_data.merge(artist_plays, 
                                                    left_on = 'artist-name', 
                                                    right_on = 'artist-name', 
                                                    how = 'left')


    popularity_threshold = 40000 # Limite inferior na colunas plays
    user_data_popular_artists = user_data_with_artist_plays.query('total_artist_plays >= @popularity_threshold')

    combined = user_data_popular_artists.merge(user_profiles, left_on = 'users', right_on = 'users', how = 'left')

    br_data = combined.query('country == \'Brazil\'')

    br_data = br_data.drop_duplicates(['users', 'artist-name'])

    br_data.to_csv("lastFM-data\\user-data.csv")

if __name__ == "__main__":
    insertDB("lastFM-data\\data.csv")
    #parse("C:\\Users\\belin\\Downloads\\lastfm-dataset-360K.tar\\lastfm-dataset-360K\\usersha1-artmbid-artname-plays.tsv","C:\\Users\\belin\\Downloads\\lastfm-dataset-360K.tar\\lastfm-dataset-360K\\usersha1-profile.tsv")