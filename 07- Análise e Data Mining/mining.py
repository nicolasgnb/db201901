import psycopg2
import matplotlib.pyplot as plt

# (string de prefixo , sql_query)
queries = [
    ("1.a)Média e Desvio Padrão dos filmes",
     """ SELECT AVG(rating), STDDEV(rating) FROM CURTEFILME """),
    ("1.b)Média e Desvio Padrão dos artistas",
     """ SELECT AVG(rating), STDDEV(rating) FROM CURTEARTISTA """),
    ("2.a)Filmes com maior rating médio",
     """SELECT  AVG(rating), movieuri FROM CURTEFILME GROUP BY movieuri HAVING COUNT(*) > 1 ORDER BY AVG(rating) DESC"""),
    ("2.b)Artistas com maior rating médio",
     """SELECT  AVG(rating), banduri FROM CURTEARTISTA GROUP BY banduri HAVING COUNT(*) > 1 ORDER BY AVG(rating) DESC"""),
    ("3.a)10 artistas mais populares",
     """SELECT banduri FROM CURTEARTISTA GROUP BY banduri ORDER BY COUNT(*) DESC LIMIT 10 """),
    ("3.b)10 filmes mais populares",
     """SELECT movieuri FROM CURTEFILME GROUP BY movieuri ORDER BY COUNT(*) DESC LIMIT 10 """),
    # ("4) View Conhece normalizada", """CREATE VIEW ConheceNormalizada AS
    #                     SELECT DISTINCT person, col FROM (
    #                         SELECT c1.person AS person, c1.colleague AS col FROM conhece c1
    #                         UNION
    #                         SELECT c2.colleague AS person, c2.person AS col FROM conhece c2
    #                     ) AS nested;"""),
    ("5)Conhecidos que compatilham filmes curtidos", """ SELECT C.person, C.colleague
                                                                FROM ConheceNormalizada C
                                                                JOIN curtefilme curte ON curte.person IN (C.person, C.colleague)
                                                                GROUP BY C.person, C.colleague
                                                                ORDER BY COUNT(*) DESC
                                                                LIMIT 1"""),
    ("6) Numero de conhecido de conhecidos para cada integrante do grupo", """ SELECT C1.person, COUNT(*)
                                                                            FROM ConheceNormalizada C1 JOIN ConheceNormalizada C2 
                                                                            ON C1.person IN ('http://utfpr.edu.br/CSB30/2019/1/DI1901nicolasmessias', 'http://utfpr.edu.br/CSB30/2019/1/DI1901feliperego', 'http://utfpr.edu.br/CSB30/2019/1/DI1901victorsilva')
                                                                                AND C2.person = C1.colleague
                                                                            GROUP BY C1.person
                                                                            ORDER BY COUNT(*) DESC"""),
    ("9.a) Top 5 pessoas que avaliaram mais filmes" , """SELECT  person, COUNT(person) AS quantidade FROM CURTEFILME GROUP BY person ORDER BY quantidade DESC LIMIT 5""" ), 
    ("9.b) Media de avaliações feitas por pessoa" , """SELECT AVG(resultado.quantidade) as media from (SELECT  person, COUNT(person) AS quantidade FROM CURTEFILME GROUP BY person ORDER BY quantidade) as resultado""")
]

graphs = [
    ("7) número de pessoas que curtiram exatamente x filmes", "pessoas", "filmes", """ SELECT COUNT(*), curtidas FROM (
                                                SELECT person, COUNT(*) AS curtidas FROM curtefilme
                                                GROUP BY person
                                            ) AS nested
                                            GROUP BY curtidas
                                            ORDER BY curtidas """),
    ("8) número de filmes curtidos por exatamente x pessoas", "pessoas", "filmes", """ SELECT cont, COUNT(*) FROM (
                                                SELECT movieuri, COUNT(*) AS cont 
                                                FROM curtefilme GROUP BY movieuri
                                            ) AS nested
                                            GROUP BY cont
                                            ORDER BY cont """)                                        
]


def print_row(row):
    for value in row:
        print(value, end='  ')


def run_queries(cursor):
    for query in queries:
        print(query[0])
        cursor.execute(query[1])
        for row in cursor.fetchall():
            print_row(row)
            print()
        print("\n")


def run_graphs(cursor):
    for graph in graphs:
        plt.title(graph[0])
        cursor.execute(graph[3])
        for row in cursor.fetchall():
            plt.scatter(row[0], row[1])
        plt.ylabel(graph[1])
        plt.xlabel(graph[2])
        plt.show()


if __name__ == "__main__":
    try:
        connection = psycopg2.connect(
            dbname="1901Team_One",
            host="200.134.10.32",
            user="1901Team_One",
            password="471737",
        )
        cursor = connection.cursor()

        run_queries(cursor)
        run_graphs(cursor)
        # plt.plot([1, 2, 3, 4], [1, 4, 9, 16])
        # plt.ylabel('some numbers')
        # plt.show()

    except psycopg2.Error as error:
        print(error.pgerror)
    finally:
        if(connection):
            cursor.close()
            connection.close()
            print("Conexão fechada.")
