import psycopg2
import pandas as pd
import numpy as np
import re
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel, pairwise_distances
from scipy.sparse.linalg import svds

MOVIE_QUERY = """
SELECT f.movieId, f.imdbId, MAX(f.titulo), array_to_string(array_agg(g.nome), '|')
FROM FILME f, GENEROFILME g, PERTENCEFILME p
WHERE f.imdbId = p.filme AND g.nome = p.genero
GROUP BY f.movieId, f.imdbId;
"""
RATINGS_QUERY = """
SELECT * FROM CurteFilme;
"""
USER_QUERY = """
SELECT userId FROM Usuario WHERE userLink LIKE '%{}';
"""

MOVIE_TITLE_QUERY = """
SELECT titulo FROM Filme WHERE titulo LIKE '%{}%';
"""


class Recommendator:
    def __init__(self, encoding, folder):
        try:
            connection = psycopg2.connect(
                dbname="1901Team_One",
                host="200.134.10.32",
                user="1901Team_One",
                password="471737",
            )
            self.cursor = connection.cursor()
            self.ratings = pd.DataFrame(self.runQuery(RATINGS_QUERY), columns=['userId', 'movieId', 'rating'])
            self.ratings[['movieId']] = self.ratings[['movieId']].astype(int)
            self.ratings[['rating']] = self.ratings[['rating']].astype(float)
            self.movies = pd.DataFrame(self.runQuery(MOVIE_QUERY), columns=['movieId', 'imdbId', 'title', 'genres'])
            self.movies[['movieId']] = self.movies[['movieId']].astype(int)

        except psycopg2.Error as error:
            print(error.pgerror)
            connection.close()

    def runQuery(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()

class MatrixFactorizationRec(Recommendator):
    def __init__(self, encoding, folder):
        super().__init__(encoding, folder)
        R_df = self.ratings.pivot(index = 'userId', columns ='movieId', values = 'rating').fillna(0)
        R = R_df.to_numpy()
        user_ratings_mean = np.mean(R, axis = 1)
        R_demeaned = R - user_ratings_mean.reshape(-1, 1)
        U, sigma, Vt = svds(R_demeaned, k = 50)
        sigma = np.diag(sigma)
        all_user_predicted_ratings = np.dot(np.dot(U, sigma), Vt) + user_ratings_mean.reshape(-1, 1)
        self.preds_df = pd.DataFrame(all_user_predicted_ratings, columns = R_df.columns)
        self.movies['genres'] = self.movies['genres'].str.split('|').fillna("").astype('str')
        self.matrix = TfidfVectorizer(analyzer='word',ngram_range=(1, 2),min_df=0, stop_words='english').fit_transform(self.movies['genres'])
        self.cosine_similarity = linear_kernel(self.matrix,  self.matrix)
        self.indices = pd.Series(self.movies.index, index=self.movies['title'])

    def recommend_movies(self, userId, num_recommendations=5):
        # Get and sort the user's predictions
        if not userId.isdigit():
            userIds = self.runQuery(USER_QUERY.format(userId))
            if userIds:
                userId = userIds[0][0]
        else:
            userId = int(userId)
        print(userId)

        user_row_number = userId - 1 # UserID starts at 1, not 0
        sorted_user_predictions = self.preds_df.iloc[user_row_number].sort_values(ascending=False) # UserID starts at 1

        # Get the user's data and merge in the movie information.
        user_data = self.ratings[self.ratings.userId == (userId)]
        user_full = (user_data.merge(self.movies, how = 'left', left_on = 'movieId', right_on = 'movieId').
                        sort_values(['rating'], ascending=False)
                    )
        # Recommend the highest predicted rating movies that the user hasn't seen yet.
        recommendations = (self.movies[~self.movies['movieId'].isin(user_full['movieId'])].
            merge(pd.DataFrame(sorted_user_predictions).reset_index(), how = 'left',
                left_on = 'movieId',
                right_on = 'movieId').
            rename(columns = {user_row_number: 'Predictions'}).
            sort_values('Predictions', ascending = False).
                        iloc[:num_recommendations, :-1]
                        )

        return recommendations

    def genre_recommendations(self, title):
        searchTitles = self.runQuery(MOVIE_TITLE_QUERY.format(title))
        if searchTitles:
            title = searchTitles[0][0]
        idx = self.indices[title]
        sim_scores = list(enumerate(self.cosine_similarity[idx]))
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
        sim_scores = sim_scores[1:6]
        movie_indices = [i[0] for i in sim_scores]
        return self.movies['imdbId'].iloc[movie_indices]

class ContentRecommendator(Recommendator):
    def __init__(self, encoding, folder):
        super().__init__(encoding, folder)
        self.movies['genres'] = self.movies['genres'].str.split('|').fillna("").astype('str')
        self.matrix = TfidfVectorizer(analyzer='word',ngram_range=(1, 2),min_df=0, stop_words='english').fit_transform(self.movies['genres'])
        self.cosine_similarity = linear_kernel(self.matrix,  self.matrix)
        self.indices = pd.Series(self.movies.index, index=self.movies['title'])

    def genre_recommendations(self, title):
        idx = self.indices[title]
        sim_scores = list(enumerate(self.cosine_similarity[idx]))
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
        sim_scores = sim_scores[1:21]
        movie_indices = [i[0] for i in sim_scores]
        return self.movies['title'].iloc[movie_indices]

# rec = MatrixFactorizationRec('latin-1', 'ml-latest-small/')

# # print(rec.genre_recommendations('Good Will Hunting (1997)').head(20))

# _, rec1 = rec.recommend_movies(300)
# print(rec1)
# _, rec2 = rec.recommend_movies(301)
# print(rec2)

# def init():
#     data = pd.read_csv(DATA_FOLDER + 'ratings.csv', sep=SEPARATOR)
#     movie_titles = pd.read_csv(DATA_FOLDER + 'movies.csv')
#     data = pd.merge(data, movie_titles, on='movieId')
#     ratings_mean_count = pd.DataFrame(data.groupby('title')['rating'].mean())
#     ratings_mean_count['rating_counts'] = pd.DataFrame(data.groupby('title')['rating'].count())
#     return data.pivot_table(index='userId', columns='title', values='rating')

# def rec_movie(movie, ratings):
#     movie_ratings = user_ratings['Forrest Gump (1994)']
#     movies_like_forest_gump = user_movie_rating.corrwith(forrest_gump_ratings)

#     corr_forrest_gump = pd.DataFrame(movies_like_forest_gump, columns=['Correlation'])
#     corr_forrest_gump.dropna(inplace=True)
#     corr_forrest_gump = corr_forrest_gump.join(ratings_mean_count['rating_counts'])
#     print(corr_forrest_gump.head())