import musicbrainzngs
import re
from urllib.request import urlopen
from urllib.parse import quote
import xml.etree.ElementTree as ET

musicbrainzngs.set_useragent("App","1")

def get(root, terms):
    e = root
    for term in terms:
        if e:
            e = e.find("{http://musicbrainz.org/ns/mmd-2.0#}" + term)
    return e

def getGenres(id):
    url = "https://musicbrainz.org/ws/2/artist/{}?inc=genres".format(id)
    xml_file = urlopen(url)
    root = ET.parse(xml_file).getroot()
    genres = []
    for genre in root.iter("{http://musicbrainz.org/ns/mmd-2.0#}genre"):
        if int(genre.get("count")) > 0:
            genres.append(genre.find("{http://musicbrainz.org/ns/mmd-2.0#}name").text)
    return genres

def getMembers(id): # cuidar membros adicionais
    url = "https://musicbrainz.org/ws/2/artist/{}?inc=artist-rels".format(id)
    xml_file = urlopen(url)
    root = ET.parse(xml_file).getroot()
    members = []
    for relation in root.iter("{http://musicbrainz.org/ns/mmd-2.0#}relation"):
        if relation.get("type") == "member of band":
            member = get(relation, ["artist"])
            uri = member.get("id")
            name = get(member, ["name"]).text
            bandUri = id
            begin = end = ""
            if get(relation, ["begin"]) != None:
                begin = get(relation, ["begin"]).text
            if get(relation, ["end"]) != None:
                end = get(relation, ["end"]).text
            members.append((name,uri,bandUri,begin,end))
    return members

def getText(root, terms):
    result = get(root, terms)
    if result != None:
        return result.text
    else:
        return ""

def getArtistById(mbid):
    base_url = "https://musicbrainz.org/ws/2/artist/{}?inc=aliases%20ratings".format(mbid)
    xml_file = urlopen(base_url)
    root = ET.parse(xml_file).getroot()
    if root[0] == None:
        print("Erro analisando XML de artista {}".format(mbid))
        exit(1)

    return root[0]

def searchArtist(name):
    result = musicbrainzngs.search_artists(artist=name)
    for artist in result['artist-list']:
        artist_id = artist['id']
        name = artist['name']
        break
    releases = musicbrainzngs.get_artist_by_id(artist_id,
        includes=["release-groups"], release_type=["album", "ep"])
    for release_group in releases["artist"]["release-group-list"]:
        release_id = release_group['id']
        break

    data = musicbrainzngs.get_release_group_image_list(release_id)
    for image in data["images"]:
        if "Front" in image["types"] and image["approved"]:
            cover_art = image["thumbnails"]["large"]
            break
    return { 'name': name, 'cover_art': cover_art, 'id': artist_id }

def getArtist(artist_name):

    artist_name = artist_name.replace('%C3%A9','é') # caso de Beyoncé
    base_url = "https://musicbrainz.org/ws/2/artist/?query={}".format(quote(artist_name))
    print("Url de busca: {}".format(base_url))
    xml_file = urlopen(base_url)

    root = ET.parse(xml_file).getroot()

    if not root or not root[0] or not root[0][0] :
        artist_name = artist_name.replace('_','')
        # TEnta novamente removendo o '_'
        base_url = "https://musicbrainz.org/ws/2/artist/?query={}".format(artist_name)
        xml_file = urlopen(base_url)
        root = ET.parse(xml_file).getroot()
    if not root or not root[0] or not root[0][0]:
        print("Erro analisando XML de artista {}".format(artist_name))
        exit(1)

    #root = metadata
    #root[0] = artist_list
    #root[0][0] = artist
    return root[0][0]

def getArtistInfoByWikipedia(wikipedia_url):
    artist_name = re.compile('wiki/(\S+)$')
    artist_name = artist_name.search(wikipedia_url).group(1)
    artist = getArtist(artist_name)

    return getArtistInfo(artist)

def getArtistInfo(artist):
    result = musicbrainzngs.get_artist_by_id(artist.get('id'),includes=["aliases","ratings"])
    artist_info = {}

    try:
        artist_info["nome_artistico"] = getText(artist, ["name"])
        artist_info["nome_real"] = getText(artist, ["name"]) # inicialmente usa o nome que encontrar como real
        artist_info["pais_origem"] = getText(artist, ["area", "name"])
        artist_info["cidade_origem"] = getText(artist, ["begin-area", "name"])
        artist_info["data_origem"] = getText(artist, ["life-span", "begin"])

        artist_info["uri"] = artist.get("id")
        artist_info["tipo"] = artist.get("type")
        artist_info["rating"] = "0" # inicialmente nao atribui rating(externo) para artista
        if "alias-list" in result["artist"]: # tenta encontrar o nome real do artista
            for alias in result["artist"]["alias-list"]:
                if "type" in alias:
                    if "Legal name" == alias["type"]:
                        artist_info["nome_real"] = alias["alias"]
        if "rating" in result["artist"]: # tenta encontrar rating(externo) do artist
            artist_info["rating"] = result["artist"]["rating"]["rating"]
        artist_info["genres"] = getGenres(artist_info["uri"])
    except:
        for elem in result['artist-list']:
            print(elem)
    return artist_info, getMembers(artist_info["uri"])

if __name__ == "__main__":
    wiki = "https://en.wikipedia.org/wiki/Beyonc%C3%A9"
    print("\nLink Wikipedia: {}".format(wiki))
    info, members = getArtistInfoByWikipedia(wiki)
    print("\nInformação obtida: {}".format(info))
    print("\n**********************************\nMembros/Grupos no qual participou: {}\n".format(members))