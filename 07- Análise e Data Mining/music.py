import pandas as pd
import numpy as np
import psycopg2
from scipy.spatial.distance import cosine
from scipy.sparse import csr_matrix
from sklearn.neighbors import NearestNeighbors

ARTISTAS_QUERY = """
SELECT a.nome_artistico FROM curteartistamusical curte, Usuario u, artistamusical a
WHERE u.userId = curte.userId AND userLink LIKE '%{userId}' AND a.mbid = curte.artista
ORDER BY curte.nota
"""

def save_sparse_csr(filename,array):
    np.savez(filename,data = array.data ,indices=array.indices,
             indptr =array.indptr, shape=array.shape )

def load_sparse_csr(filename):
    loader = np.load(filename)
    return csr_matrix((loader['data'], loader['indices'], loader['indptr']),
                         shape = loader['shape'])

def train():
    pd.set_option('display.float_format', lambda x: '%.3f' % x)
    user_data = pd.read_csv('/home/nmessias/Documents/lastfm-dataset-360K/usersha1-artmbid-artname-plays.tsv',
                        sep='\t', header = None, nrows = 2e7,
                        names = ['users', 'musicbrainz-artist-id', 'artist-name', 'plays'],
                        usecols=['users', 'artist-name', 'plays'])
    user_profiles = pd.read_csv('/home/nmessias/Documents/lastfm-dataset-360K/usersha1-profile.tsv',
                        header = None, sep='\t',
                        names = ['users', 'gender', 'age', 'country', 'signup'],
                        usecols = ['users', 'country'])
    if user_data['artist-name'].isnull().sum() > 0:
        user_data = user_data.dropna(axis = 0, subset = ['artist-name'])
    artist_plays = (user_data.
        groupby(by = ['artist-name'])['plays'].
        sum().
        reset_index().
        rename(columns = {'plays': 'total_artist_plays'})
        [['artist-name', 'total_artist_plays']]
    )

    user_data_with_artist_plays = user_data.merge(artist_plays, left_on = 'artist-name', right_on = 'artist-name', how = 'left')
    popularity_threshold = 40000
    user_data_popular_artists = user_data_with_artist_plays.query('total_artist_plays >= @popularity_threshold')

    combined = user_data_popular_artists.merge(user_profiles, left_on = 'users', right_on = 'users', how = 'left')
    br_data = combined.query('country == \'Brazil\'')
    if not br_data[br_data.duplicated(['users', 'artist-name'])].empty:
        br_data = br_data.drop_duplicates(['users', 'artist-name'])

    br_data = br_data.drop('country', 1).drop('total_artist_plays', 1)
    wide_artist_data = br_data.pivot(index = 'artist-name', columns = 'users', values = 'plays').fillna(0)
    wide_artist_data_sparse = csr_matrix(wide_artist_data.values)
    save_sparse_csr('lastFM-data/lastfm_sparse_artist_matrix.npz', wide_artist_data_sparse)
    br_data.to_csv('lastFM-data/br_data.csv', sep=',', index=None, header=None)

class MusicRecommender:
    def __init__(self, folder):
        connection = psycopg2.connect(
                dbname="1901Team_One",
                host="200.134.10.32",
                user="1901Team_One",
                password="471737",
            )
        self.cursor = connection.cursor()
        self.user_data = pd.read_csv(folder + 'br_data.csv', sep=',', header=None,
                            names = ['users', 'artist-name', 'plays'])
        self.artist_plays_matrix = self.user_data.pivot(index = 'artist-name', columns = 'users', values = 'plays').fillna(0).apply(np.sign)
        self.knn_model = NearestNeighbors(metric='cosine', algorithm='brute')
        self.knn_model.fit(load_sparse_csr(folder + 'lastfm_sparse_artist_matrix.npz'))

    def runQuery(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def itemRec(self, query_artist, k = 5):
        query_index = self.artist_plays_matrix.index.tolist().index(query_artist.lower())
        distances, indices = self.knn_model.kneighbors(self.artist_plays_matrix.iloc[query_index, :].values.reshape(1, -1), n_neighbors = k + 1)
        recommendations = []
        for i in range(0, len(distances.flatten())):
            if i == 0:
                continue
            else:
                recommendations.append(self.artist_plays_matrix.index[indices.flatten()[i]])
        return recommendations

    def userRec(self, userId, k = 5):
        artists = self.runQuery(ARTISTAS_QUERY.format(userId=userId, k=k))
        recommendations = []
        for artist in artists:
            try:
                recommendations.append(self.itemRec(artist[0], 1)[0])
            except Exception as e:
                print(e)
                pass

        return recommendations
