import psycopg2
import scraping
import re
from urllib.request import urlopen
from xml.dom.minidom import parse, parseString
import xml.dom.minidom
import csv

USER_INSERT = """
INSERT INTO USUARIO (userLink, nome, cidade, data_nascimento) VALUES (%s,%s,%s,%s);
"""
ARTIST_INSERT = """
INSERT INTO ARTISTAMUSICAL VALUES (%s,%s,%s,%s,%s,%s,%s);
"""

BAND_INSERT = """
INSERT INTO BANDA VALUES (%s,%s,%s);
"""

BAND_MEMBER_INSERT = """
INSERT INTO MEMBROSDEBANDA VALUES (%s,%s,%s,%s,%s)
"""


MUSICIAN_INSERT = """
INSERT INTO MUSICO VALUES (%s,%s,%s,%s);
"""

GENRE_INSERT = """
INSERT INTO GENEROMUSICAL (nome) VALUES (%s);
"""

GENRE_ARTIST_INSERT = """
INSERT INTO PERTENCEMUSICAL VALUES (%s,%s);
"""

LIKES_ARTIST_INSERT = """
INSERT INTO CURTEARTISTAMUSICAL VALUES ((SELECT userId FROM Usuario WHERE userLink = %s),%s,%s);
"""

MOVIE_INSERT = """
INSERT INTO FILME VALUES ({},'{}','{}');
"""

GENRE_MOVIE_INSERT = """
INSERT INTO GENEROFILME VALUES ('{}');
"""

PERTENCE_INSERT = """
INSERT INTO PERTENCEFILME VALUES ({}, '{}');
"""

USUARIO_INSERT = """
INSERT INTO USUARIO (userId) VALUES ({});
"""

MOVIE_CURTE_INSERT = """
INSERT INTO CURTEFILME (uri, filme, nota) VALUES ({}, (SELECT imdbid FROM Filme f WHERE f.movieid = '{}'), {} );
"""

MOVIE2_CURTE_INSERT = """
INSERT INTO CURTEFILME (uri, filme, nota) VALUES ((SELECT userId FROM Usuario WHERE userLink = '{}'), '{}', {} );
"""

USUARIO2_INSERT = """
INSERT INTO USUARIO (userId, userLink) VALUES ({}, '{}');
"""

def execute_query(conn, query, param):
    try:
        conn.cursor().execute(query, param)
        conn.commit()
        return True
    except psycopg2.Error as e:
        if int(e.pgcode) != 23505:
            print(e.pgerror, "Código do erro: ",e.pgcode)
            print(query, param)
            exit(1)

def getXML():
    urls = ["http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml",
            "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml",
            "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml",
            "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml"]
    data = []
    for url in urls:
        file = urlopen(url)
        data.append(parse(file).toxml())
        file.close()
    return data

def readMovieLensCSV(conn, path):
    movies = []
    queryMovie = ''
    queryGenres = ''
    queryPertence = ''
    queryCurte = ''
    queryUser = ''
    with open(path + "movies.csv") as moviesfile:
        reader = csv.DictReader(moviesfile)
        for row in reader:
            movies.append(row)
    with open(path + "links.csv") as linksfile:
        reader = csv.DictReader(linksfile)
        for i, row in enumerate(reader):
            movie = movies[i]
            queryMovie += MOVIE_INSERT.format(row['imdbId'], movie['movieId'], re.escape(movie['title']), movie['movieId'])
            genres = movie['genres'].split('|')
            for genre in genres:
                if genre == '(no genres listed)':
                    genre  = 'no genres listed'
                queryGenres += GENRE_MOVIE_INSERT.format(genre)
                queryPertence += PERTENCE_INSERT.format(row['imdbId'], genre)
    with open(path + "ratings.csv") as ratingsfile:
        reader = csv.DictReader(ratingsfile)
        for row in reader:
            execute_query(conn, MOVIE_CURTE_INSERT.format(row['userId'], row['movieId'], row['rating']), None)
            queryCurte += MOVIE_CURTE_INSERT.format(row['userId'], row['movieId'], row['rating'])

    execute_query(conn, queryMovie, None)
    execute_query(conn, queryGenres, None)
    execute_query(conn, queryPertence, None)
    execute_query(conn, queryUser, None)
    execute_query(conn, queryCurte, None)


def insertPerson(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    persons = DOMTree.documentElement.getElementsByTagName("Person")
    for person in persons:
        uri = person.getAttribute("uri")
        name = person.getAttribute("name")
        hometown = person.getAttribute("hometown")
        birthdate = person.getAttribute("birthdate")
        if uri and name and hometown and birthdate:
            execute_query(conn, USER_INSERT, (uri,name,hometown.capitalize(),birthdate) )

def insertMusicArtist(conn, info, members):
    execute_query(conn, ARTIST_INSERT, (info["uri"],info["cidade_origem"],info["pais_origem"],info["nome_artistico"],info["data_origem"],info["rating"],0) )
    for genre in info["genres"]:
        execute_query(conn, GENRE_INSERT, (genre, ))
        execute_query(conn, GENRE_ARTIST_INSERT, (info["uri"],genre))
    if info["tipo"] == "Group":
        execute_query(conn, BAND_INSERT, (info["nome_artistico"],info["uri"],info["rating"]) )
        for member in members:
            execute_query(conn, BAND_MEMBER_INSERT, member )
    elif info["tipo"] == "Person":
        execute_query(conn, MUSICIAN_INSERT, (info["uri"],info["nome_real"],info["data_origem"],info["rating"]) )

def insertLikesMusic(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    musics = DOMTree.documentElement.getElementsByTagName("LikesMusic")
    for music in musics:
        person = music.getAttribute("person")
        rating = music.getAttribute("rating")
        wiki_url = music.getAttribute("bandUri")
        info, members = scraping.getArtistInfoByWikipedia(wiki_url)
        print(wiki_url)
        insertMusicArtist(conn, info, members)
        execute_query(conn, LIKES_ARTIST_INSERT, (person,info["uri"],rating) )

def insertLikesMovie(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    movies = DOMTree.documentElement.getElementsByTagName("LikesMovie")
    queryCurte = ''
    for movie in movies:
        person = movie.getAttribute("person")
        rating = movie.getAttribute("rating")
        imdbUri = re.compile('\/title\/tt(.+?)\/$').search(movie.getAttribute("movieUri")).group(1)
        queryCurte += MOVIE2_CURTE_INSERT.format(person, int(imdbUri), rating)
    execute_query(conn, queryCurte, None)

if __name__ == "__main__":
    try:
        connection = psycopg2.connect(
            dbname="1901Team_One",
            host="200.134.10.32",
            user="1901Team_One",
            password="471737",
        )

        connection.autocommit = True
        cursor = connection.cursor()
        #readMovieLensCSV(connection, 'ml-latest-small/')
        data = getXML()
        #insertLikesMovie(connection, data[2])
       # print("Inserindos pessoas")
       # insertPerson(connection,data[0])
        print("Inserindo musicas")
        insertLikesMusic(connection,data[1])

    except psycopg2.Error as error:
        print(error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            print("Conexão fechada.")
