﻿CREATE TABLE Usuario(
  userId SERIAL,
  userLink VARCHAR(100) UNIQUE,
  nome VARCHAR(100),
  cidade VARCHAR(100),
  data_nascimento DATE,
  PRIMARY KEY(userId)
);

CREATE TABLE Conhece(
  user1 INT,
  user2 INT,
  PRIMARY KEY(user1, user2),
  FOREIGN KEY(user1)
    REFERENCES Usuario(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(user2)
    REFERENCES Usuario(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE GeneroMusical(
  nome VARCHAR(100) NOT NULL,
  PRIMARY KEY(nome)
);

CREATE TABLE ArtistaMusical(
  mbid VARCHAR(100) NOT NULL,
  cidade_origem VARCHAR(100),
  pais_origem VARCHAR(100),
  nome_artistico VARCHAR(100),
  data_origem VARCHAR(100),
  nota VARCHAR(20),
  quantidade_tocado INT
  PRIMARY KEY(mbid)
);



CREATE TABLE Banda(
    nome VARCHAR(100) NOT NULL,
    mbid VARCHAR(100) NOT NULL,
    rating_api VARCHAR(20),
    PRIMARY KEY(mbid),
    FOREIGN KEY(mbid)
      REFERENCES ArtistaMusical(mbid)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE MembrosDeBanda(
    nome VARCHAR(100) NOT NULL,
    mbid VARCHAR(100) NOT NULL,
    banda_pertencente VARCHAR(100) NOT NULL,
    data_inicio VARCHAR(100),
    data_final VARCHAR(100),
    PRIMARY KEY(mbid, banda_pertencente),
    FOREIGN KEY(banda_pertencente)
      REFERENCES Banda(mbid)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);


CREATE TABLE Musico(
  mbid VARCHAR(100) NOT NULL,
  nome_real VARCHAR(100),
  data_nascimento VARCHAR,
  rating_api VARCHAR(20),
  PRIMARY KEY(mbid),
  FOREIGN KEY(mbid)
    REFERENCES ArtistaMusical(mbid)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE CurteArtistaMusical(
  userId INT,
  artista VARCHAR(100) NOT NULL,
  nota DECIMAL(5, 1) NOT NULL,
  PRIMARY KEY(userId, artista),
  FOREIGN KEY(userId)
    REFERENCES Usuario(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(artista)
    REFERENCES ArtistaMusical(mbid)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE PertenceMusical(
    artista VARCHAR(100) NOT NULL,
    genero VARCHAR(100) NOT NULL,
    PRIMARY KEY(genero, artista),
    FOREIGN KEY(artista)
        REFERENCES ArtistaMusical(mbid)
          ON DELETE CASCADE
          ON UPDATE CASCADE,
    FOREIGN KEY(genero)
        REFERENCES GeneroMusical(nome)
          ON DELETE CASCADE
          ON UPDATE CASCADE
);


CREATE TABLE GeneroFilme(
  nome VARCHAR(100) NOT NULL,
  PRIMARY KEY(nome)
);

CREATE TABLE ArtistaFilme(
  uri VARCHAR(100) NOT NULL,
  telefone VARCHAR(100),
  endereco VARCHAR(100),
  PRIMARY KEY(uri)
);

CREATE TABLE Filme(
    imdbId VARCHAR(100) NOT NULL,
    movieId VARCHAR(100) NOT NULL,
    titulo VARCHAR NOT NULL,
    PRIMARY KEY(imdbId)
);

CREATE TABLE Diretor(
    uri VARCHAR(100) NOT NULL,
    filme_dirigido VARCHAR(100) NOT NULL,
    PRIMARY KEY(uri, filme_dirigido),
    FOREIGN KEY(uri)
        REFERENCES ArtistaFilme(uri)
          ON DELETE CASCADE
          ON UPDATE CASCADE,
    FOREIGN KEY(filme_dirigido)
        REFERENCES FILME(imdbId)
          ON DELETE CASCADE
          ON UPDATE CASCADE
);

CREATE TABLE Ator(
    uri VARCHAR(100) NOT NULL,
    filme_atuado VARCHAR(100) NOT NULL,
    PRIMARY KEY(uri, filme_atuado),
    FOREIGN KEY(uri)
        REFERENCES ArtistaFilme(uri)
          ON DELETE CASCADE
          ON UPDATE CASCADE,
    FOREIGN KEY(filme_atuado)
        REFERENCES FILME(imdbId)
          ON DELETE CASCADE
          ON UPDATE CASCADE
);



CREATE TABLE CurteFilme(
  uri INT,
  filme VARCHAR(100) NOT NULL,
  nota DECIMAL(5, 1) NOT NULL,
  PRIMARY KEY(uri, filme),
  FOREIGN KEY(uri)
    REFERENCES Usuario(userId)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(filme)
    REFERENCES Filme(imdbId)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);

CREATE TABLE PertenceFilme(
  filme VARCHAR(100) NOT NULL,
  genero VARCHAR(100) NOT NULL,
  PRIMARY KEY(filme, genero),
  FOREIGN KEY(filme)
    REFERENCES Filme(imdbId)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
  FOREIGN KEY(genero)
    REFERENCES GeneroFilme(nome)
      ON DELETE CASCADE
      ON UPDATE CASCADE
);
