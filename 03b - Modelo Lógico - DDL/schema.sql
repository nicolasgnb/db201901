CREATE TABLE Usuario(
  login VARCHAR(60) NOT NULL,
  cidade_natal VARCHAR(60),
  nome_completo VARCHAR(60),
  PRIMARY KEY(login)
);

CREATE TABLE Conhece(
  user1 VARCHAR(60) NOT NULL,
  user2 VARCHAR(60) NOT NULL,
  PRIMARY KEY(user1, user2),
  FOREIGN KEY(user1)
    REFERENCES Usuario(login),
  FOREIGN KEY(user2)
    REFERENCES Usuario(login)
);

CREATE TABLE Bloqueia(
  user1 VARCHAR(60) NOT NULL,
  user2 VARCHAR(60) NOT NULL,
  razao_bloqueio VARCHAR(100) NOT NULL,
  PRIMARY KEY(user1, user2),
  FOREIGN KEY(user1)
    REFERENCES Usuario(login),
  FOREIGN KEY(user2)
    REFERENCES Usuario(login)
);

CREATE TABLE Artista_musical(
  identificador VARCHAR(60) NOT NULL,
  genero_musical VARCHAR(60) NOT NULL,
  pais VARCHAR(60) NOT NULL,
  nome_artistico VARCHAR(60) NOT NULL,
  PRIMARY KEY(identificador)
);

CREATE TABLE Dupla(
  nome VARCHAR(60) NOT NULL,
  identificador VARCHAR(60) NOT NULL,
  PRIMARY KEY(nome, identificador),
  FOREIGN KEY(identificador)
    REFERENCES Artista_musical(identificador)
);

CREATE TABLE Grupo(
  nome VARCHAR(60) NOT NULL,
  identificador VARCHAR(60) NOT NULL,

  PRIMARY KEY(nome, identificador),
  FOREIGN KEY(identificador)
    REFERENCES Artista_musical(identificador)
);

CREATE TABLE Musico(
  identificador VARCHAR(60) NOT NULL,
  estilo_musical VARCHAR(60) NOT NULL,
  nome_real VARCHAR(60) NOT NULL,
  data_nascimento DATE NOT NULL,
  grupo VARCHAR(60),
  dupla VARCHAR(60),
  PRIMARY KEY(identificador),
  FOREIGN KEY(grupo, identificador)
    REFERENCES Grupo(nome, identificador),
  FOREIGN KEY(dupla, identificador)
    REFERENCES Dupla(nome, identificador),
  FOREIGN KEY(identificador)
    REFERENCES Artista_musical(identificador)
);

CREATE TABLE Categoria(
  nome VARCHAR(60) NOT NULL,
  supercategoria VARCHAR(60),
  PRIMARY KEY(nome),
  FOREIGN KEY(supercategoria)
    REFERENCES Categoria(nome)
);

CREATE TABLE Artista_filme(
  identificador VARCHAR(60) NOT NULL,
  telefone VARCHAR(60) NOT NULL,
  endereco VARCHAR(60) NOT NULL,
  PRIMARY KEY(identificador)
);

CREATE TABLE Diretor(
  identificador VARCHAR(60) NOT NULL,
  PRIMARY KEY(identificador),
  FOREIGN KEY(identificador)
    REFERENCES Artista_filme(identificador)
);

CREATE TABLE Ator(
  identificador VARCHAR(60) NOT NULL,
  PRIMARY KEY(identificador),
  FOREIGN KEY(identificador)
    REFERENCES Artista_filme(identificador)
);

CREATE TABLE Filme(
  identificador VARCHAR(60) NOT NULL,
  nome VARCHAR(60) NOT NULL,
  data_lancamento DATE NOT NULL,
  diretor VARCHAR(60) NOT NULL,
  salario REAL NOT NULL,
  PRIMARY KEY(identificador),
  FOREIGN KEY(diretor)
    REFERENCES Diretor(identificador)
);

CREATE TABLE Atua(
  ator VARCHAR(60) NOT NULL,
  filme VARCHAR(60) NOT NULL,
  salario REAL NOT NULL,
  PRIMARY KEY(ator, filme),
  FOREIGN KEY(ator)
    REFERENCES Ator(identificador),
  FOREIGN KEY(filme)
    REFERENCES Filme(identificador)
);

CREATE TABLE CurteFilme(
  login VARCHAR(60) NOT NULL,
  filme VARCHAR(60) NOT NULL,
  nota DECIMAL(4, 2) NOT NULL,
  PRIMARY KEY(login, filme),
  FOREIGN KEY(login)
    REFERENCES Usuario(login),
  FOREIGN KEY(filme)
    REFERENCES Filme(identificador)
);

CREATE TABLE CurteArtista(
  login VARCHAR(60) NOT NULL,
  artista VARCHAR(60) NOT NULL,
  nota DECIMAL(4, 2) NOT NULL,
  PRIMARY KEY(login, artista),
  FOREIGN KEY(login)
    REFERENCES Usuario(login),
  FOREIGN KEY(artista)
    REFERENCES Ator(identificador)
);


CREATE TABLE Pertence(
  filme VARCHAR(60) NOT NULL,
  categoria VARCHAR(60) NOT NULL,
  PRIMARY KEY(filme, categoria),
  FOREIGN KEY(filme)
    REFERENCES Filme(identificador),
  FOREIGN KEY(categoria)
    REFERENCES Categoria(nome)
);
