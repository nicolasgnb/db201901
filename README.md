
# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

## Integrantes do grupo

- Nícolas Vilela Messias, 1984101, nicolasgnb
- Victor Hugo Belinello, 1796526, victorbelinello
- Felipe Avelino, 1504738, fewiip

# Definição do tema da última fase do trabalho

- O que vocês vão implementar:
    Iremos implementar um sistema de recomendação, através de uma interface WEB, de filmes e músicas utilizando os dados fornecidos e outro complementares obtidos externamente.
- Quais tecnologias vão utilizar:
    Iremos manter o SGBD relacional PostgreSQL que atendará os requisitos do projeto. Iremos manter a linguagem em Python, mas utilizaremos a microframework Flask para auxiliar no do web server e a framerwork React para o front-end do projeto.
- Por que decidiram pelo tema:
    Foi escolhido esse tema devido ao interesse de ambos os membros no tema de Machine Learning e correlatos, além da crescente demanda de conhecimento e aplicações nessa área.
- Quais os desafios relacionados com bancos de dados:
    Em uma análise primária do projeto não foram identificados problemas acima do nível apresentado durante as aulas
- Quais os desafios relacionados com suas áreas de interesse:
    No aspectode de recomendações o principal problema é a inexperiência dos membros na área, ninguém trabalhou na área seja em aspecto teórico, seja prático. Já na interface WEB temos experiência o suficiente para não apresentar nenhum desafio.
- Quais conhecimentos novos vocês pretendem adquirir:
    Além do conhecimento na área de Machine Learning, necessário para a parte de recomendação, um membro (Victor) deverá desenvolver conhecimentos no aspecto de programação WEB. Além disso para obtenção de novos dados deveremos aprender a utilizar as diversas APIs dos fornecedores de dados utilizado(IMDB, MusicBrainz etc)
- O que voces já produziram:
    Já aprimoramos o banco de dados, no que diz respeito ao aprimoramento dos esquemas para terem mais informações úteis para a recomendação. Também já implementamos as principais interfaces para obtenção de dados externos. Um prototipo de recomendações de filmes baseado no dataset MovieLens foi desenvolvido para realizar a fatoração de matrizes baseado em ratings de filmes.

# Informações específicas:

- Victor Hugo
    - O foco será na parte de recomendação, obtenção de dados e correlatos para as músicas/bandas/artistas com ênfase no back-end e interação com o banco
    - Experiência apenas obtida nas aulas e um conhecimento básico de Django e WEB em geral
    - Interesse maior na parte de Machine Learning e em seguida na implementação em baixo nível do SGBD, apesar de não abordarmos esse aspecto no trabalho.

- Nícolas Messias
    - Focarei na parte de recomendação, especificamente para recomendações de filmes.
    - Tenho experiencia no desenvolvimento WEB, principalmente no React. O resto da experciena sobre banco de dados vêm do curso em si e o uso diario que tenho em meu trabalho.
    - Grande interesse na parte do sistema de recomendações, uma introdução para a área de Machine Learning é bem legal.
