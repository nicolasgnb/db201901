import urllib.request
import psycopg2
from xml.dom.minidom import parse
import xml.dom.minidom


def get_data_xml():
    person_url = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml"
    music_url = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml"
    movie_url = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml"
    knows_url = "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml"

    person_file = urllib.request.urlopen(person_url)
    person_data = parse(person_file).toxml()
    person_file.close()

    music_file = urllib.request.urlopen(music_url)
    music_data = parse(music_file).toxml()
    music_file.close()

    movie_file = urllib.request.urlopen(movie_url)
    movie_data = parse(movie_file).toxml()
    movie_file.close()

    knows_file = urllib.request.urlopen(knows_url)
    knows_data = parse(knows_file).toxml()
    knows_file.close()

    return person_data, music_data, movie_data, knows_data


def connect_db_rle():
    conn = ''
    try:
        conn = psycopg2.connect(
            "dbname='1901Team_One' user='1901Team_One' host='200.134.10.32' password='471737'")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}').format(e)
    return conn


def insert(conn, query):
    cur = conn.cursor()
    try:
        cur.execute(query)
    except Exception as e:
        print("I can't insert table!")
        print(e)


def insert_person_database(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    pessoas = DOMTree.documentElement.getElementsByTagName("Person")
    for pessoa in pessoas:
        uri = pessoa.getAttribute("uri")
        name = pessoa.getAttribute("name")
        hometown = "'" + \
            pessoa.getAttribute(
                "hometown") + "'" if pessoa.getAttribute("hometown") != "" else "NULL"
        birthdate = "'" + \
            pessoa.getAttribute(
                "birthdate") + "'" if pessoa.getAttribute("hometown") != "" else "NULL"
        insert_query = "INSERT INTO Usuario (uri, hometown, name, birthdate) VALUES ('{0}', {1}, '{2}', {3})".format(
            uri, hometown, name, birthdate)
        insert(conn, insert_query)
    conn.commit()


def insert_like_music_database(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    likes = DOMTree.documentElement.getElementsByTagName("LikesMusic")
    for like in likes:
        person = like.getAttribute("person")
        rating = like.getAttribute("rating")
        bandUri = like.getAttribute("bandUri")
        insert_query = "INSERT INTO CurteArtista (person, rating, bandUri) VALUES (\'" + person + \
            "\', \'" + rating + "\', \'" + bandUri + "\');"
        insert(conn, insert_query)
    pass
    conn.commit()


def insert_like_movie_database(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    likes = DOMTree.documentElement.getElementsByTagName("LikesMovie")
    for like in likes:
        person = like.getAttribute("person")
        rating = like.getAttribute("rating")
        movieUri = like.getAttribute("movieUri")
        insert_query = "INSERT INTO CurteFilme (person, rating, movieUri) VALUES (\'" + person + \
            "\', \'" + rating + "\', \'" + movieUri + "\');"
        insert(conn, insert_query)
    pass
    conn.commit()


def insert_knows_database(conn, data):
    DOMTree = xml.dom.minidom.parseString(data)
    knows = DOMTree.documentElement.getElementsByTagName("Knows")
    for know in knows:
        person = know.getAttribute("person")
        colleague = know.getAttribute("colleague")
        insert_query = "INSERT INTO Conhece (person, colleague) VALUES (\'" + person + \
            "\', \'" + colleague + "\');"
        insert(conn, insert_query)
    pass
    conn.commit()


def main():
    pe_data, mu_data, mv_data, kn_data = get_data_xml()
    conn = connect_db_rle()
    insert_person_database(conn, pe_data)
    insert_like_music_database(conn, mu_data)
    insert_like_movie_database(conn, mv_data)
    insert_knows_database(conn, kn_data)


if __name__ == '__main__':
    main()
