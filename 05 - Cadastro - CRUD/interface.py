# coding=utf-8

import psycopg2
import psycopg2.extras


def connect_db_rle():
    conn = ''
    try:
        conn = psycopg2.connect(
            "dbname='1901Team_One' user='1901Team_One' host='200.134.10.32' password='471737'")
    except psycopg2.OperationalError as e:
        print('Unable to connect!\n{0}'.format(e))
    return conn


def execute_query(conn, query):
    cur = conn.cursor()
    try:
        cur.execute(query)
        conn.commit()
    except Exception as e:
        print("I can't execute the query!")
        print(e)


def get_users(conn):
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        cur.execute("SELECT * FROM Usuario;")
    except psycopg2.DatabaseError as e:
        print("Unable to select!\n{0}".format(e))
    return cur.fetchall()


def register_user(conn, uri, name, birthdate, hometown):
    insert_query = "INSERT INTO Usuario (uri, hometown, name, birthdate) VALUES ('{0}', '{1}', '{2}', '{3}')".format(
        uri, hometown, name, birthdate)
    execute_query(conn, insert_query)

def delete_user(conn, uri):
    query = "DELETE FROM Usuario WHERE uri = '{0}'".format(uri)
    execute_query(conn, query)

def update_user(conn, uri, nome, cidade, data):
    query = "UPDATE Usuario SET "
    if nome != "":
        query += "name='{}'".format(nome)
    if cidade != "":
        query += ",hometown='{}'".format(cidade)
    if data != "":
        query += ",birthdate='{}'".format(data)
    query += " WHERE uri='{}'".format(uri)
    execute_query(conn, query)

def main():
    conn = connect_db_rle()
    print("Escolha uma opção: ")
    print("1) Cadastrar uma pessoa;")
    print("2) Listar todas pessoas. ")
    opcao = input()
    if opcao == '1':
        uri = input("Entre com o URI: ")
        name = input("Entre com o nome: ")
        hometown = input("Entre com a cidade natal: ")
        birthdate = input("Entre com o birthdate: ")
        register_user(conn, uri, name, birthdate, hometown)
        print("Pessoa cadastrada.")

    elif opcao == '2':
        users = get_users(conn)
        for user in users:
            print("URI: {0}\nNome: {1}\nCidade natal: {2}\nData de nascimento: {3}\n\n".format(
                user[0], user[2], user[1], user[3]))
        print("Escolha uma opção: ")
        print("1) Apagar uma pessoa;")
        print("2) Editar uma pessoa. ")
        opcao = input()
        uri = input("Entre com o URI da pessoa: ")
        if opcao == '1':
            delete_user(conn, uri)
            print("Pessoa apagada.")
        elif opcao == '2':
            nome = input("Digite o novo nome: ")
            cidade = input("Digite a nova cidade: ")
            data = input("Digite a nova data de nascimento(AAAA-MM-DD): ")
            update_user(conn, uri, nome, cidade, data)
            print("Pessoa alterada.")


if __name__ == '__main__':
    main()
