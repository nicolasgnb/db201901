#!/usr/bin/python3

from xml.dom.minidom import parse
import xml.dom.minidom
import os


def get_attribute(hero, attribute_name):
    return hero.getElementsByTagName(attribute_name)[0].childNodes[0].data


def imc(hero):
    weight = int(get_attribute(hero, "weight_kg"))
    height = int(get_attribute(hero, "height_m"))
    return str(weight / (height * height)) + "kg/m^2"


# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement # Primeira tag

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")

try: # Make dir dadosMarvel on current path (of .py file)
    os.mkdir("dadosMarvel")
except FileExistsError:
    pass

# Create/Open csv file
herois = open("dadosMarvel/herois.csv", "w")
herois_good = open("dadosMarvel/herois_good.csv", "w")
herois_bad = open("dadosMarvel/herois_bad.csv", "w")
attribute_names = ["name", "popularity", "alignment", "gender", "height_m", "weight_kg", "hometown",
                   "intelligence", "strength", "speed", "durability", "energy_Projection", "fighting_Skills"]
cont_good = 0
cont_bad = 0
cont = 0
peso_total = 0
for hero in heroes:
    line = ""
    cont += 1
    attributes = []
    if hero.hasAttribute("id"):
        line += hero.getAttribute("id") + ", "
    if get_attribute(hero, "name") == "Hulk":
        print("IMC Hulk: " + imc(hero))

    for name in attribute_names:
        attributes.append(get_attribute(hero, name))
    line += ', '.join(attributes)
    line += '\n'

    herois.write(line)
    if attributes[2] == "Good":
        herois_good.write(line)
        cont_good += 1
    elif attributes[2] == "Bad":
        herois_bad.write(line)
        cont_bad += 1

    peso_total += int(get_attribute(hero, "weight_kg"))
    media_peso = peso_total / cont;
        

print("Proporção heróis bons/maus: " + str(cont_good/cont_bad))
print("Media dos pesos: " + str(media_peso))
